# Self-Description Distribution

This document is about the management of distributed state in a distributed catalogue system.

## Registry

The "public lifecycle" of Self-Descriptions is handled on the so-called Registry.
The Registry is an event layer where Providers can announce changes to a Catalogue.

![State Transitions](figures/sd-state-diagram.png)

All state transitions described in the above figure are triggered by a message that is distributed over the registry.
With the exception of the transition to the EOL (end-of-life) state, that is time-based and does not require a dedicated message on the Registry.

The Self-Description state transition messages carry the following attributes.
The message is to be encoded as JSON-LD. This may add additional meta-data that is not considered here.

    {
	    "location": "http://my-domain/my-self-description",
	    "entity": "gaiax:my-entity-id",
	    "hash": "9F86D081884C7D659A2FEAA0C55AD015A3BF4F1B2B0B822CD15D6C15B0F00A08",
	    "issuer": "gaiax:participant-id",
	    "issue-date": "2012-04-23T18:25:43.511Z",
	    "status": active | inactive | revoked,
	    "reason": "..." (optional)
    }
    
- The `location` attribute describes the URL at which the Self-Description can be downloaded via http(s).
  The messages sent over the Registry contain links where Self-Descriptions can be received.
  The location might be public or protected by additional access control mechanisms.
- The `entity` attribute contains the unique identifier of the entity that is described in the Self-Description.
- The `hash` attribute contains the SHA256 hash of the Self-Description content (including any proofs added to the Self-Descriptions actual payload).
- The `issuer` attribute contains the unique identifier of the GAIA-X participant that has created the Self-Description.
- The `issue-date` attribute contains a string with the ISO 8601-formatted date-string at which the Self-Description was issued.
- The `status` attribute contains either of the strings "active", "inactive" or "revoked".
  The deprecated state is indicated by the publication of a newer Self-Description (with a different hash and a later issue-date) for the same entity.
- The `reason` attribute is optional and contains a human-readable textual description why the state transition was triggered.

The messages have to be signed so that the content is trusted.

## Filtering of Events and Self-Descriptions

## Conflict Resolution
