# Self-Description Lifecycle

This document is about states and state transitions of self-descriptions.

The claims of the Self-Descriptions in JSON-LD format can carry cryptographic
proofs based on hash signatures. Furthermore, the hash of the JSON-LD file can
be used as an identifier to reference to the source of information from the
Catalogue.
Hence the JSON-LD files are immutable and can only be replaced as a
whole. The state of the Self-Descriptions therefore needs to be tracked as
metadata outside of the JSON-LD files itself.

Since Self-Descriptions are protected by cryptographic signatures, they are immutable and cannot be changed once published.
This implies that after any changes to a Self-Description, the Participant as the Self-Description issuer has to sign the Self-Description again and release it as a new version.

## Self-Description Storage

Every Catalogue in a Federated System has a local Self-Description storage (SD Storage).
The SD Storage contains the raw Self-Descriptions in the JSON-LD format and additional lifecycle meta-data (as signed Self-Descriptions are immutable).
The local SD Storage can be considered a cache and not an authorative source of Self-Descriptions.
The original source of Self-Descriptions are the Providers.

Search queries are answered by the catalogue based on the "active set" of Self-Descriptions in the local SD Storage that have the "active" lifecycle state.
For that, the Self-Descriptions in the active set are imported in the Self-Description Graph that enables queries across multiple Self-Descriptions.

Before changes are applied to the SD Storage, the Catalogue verifies the change against the locally available information.
The user of the Catalogue is empowered to replicate the verification steps in a Self-Sovereign manner.
For this, all relevant data (Self-Descriptions, trust anchors, etc., which are all locally available) are made available to users of the Catalogue via its API.

The locally available information in the Catalogue comprise at least:

- Active Self-Description Schemas
- The other Self-Descriptions (including metadata) in the local storage (cache)
- Trust information (e.g. trustlist/issuerlist/revocationlist) configured in the Catalogue

## Lifecycle and Versioning

The Self-Description has a "public lifecycle" that is managed by its issuer.
Only active Self-Descriptions are considered by the Catalogue.

- Active
- Inactive
- EOL (end of life after a timeout date)
- Deprecated (by a newer Self-Description)
- Revoked (by a trusted party)

Inactive: The issuer wishes that the Self-Description is not loaded into the
Self-Description Graph for querying. Inactive is the only state that can return
to an active state.

End of Life: The Self-Descriptions are the source information for the GAIA-X
Catalogue. In order for the Catalogue to contain only up-to-date information it
should be "self-cleaning" whereby outdated information is removed automatically.
This can be achieved by timeout dates attached to every Self-Description after
which they are end-of-life. It is recommended that the automatic timeout date of
Self-Descriptions is set rather low, e.g., 90 days. This has proven useful in
the context of TLS certificates (e.g. Let's Encrypt) where a frequent renewal
forces providers that automated update systems are put in place instead of
infrequent manual updates.

Deprecated: If two versions of GAIA-X Assets (Services, Data etc.) are offered
at the same time, then each version is described by an independent
Self-Description with a unique identifier. If the Self-Description itself
requires an update, the entire JSON-LD file is published in an updated version
and the old Self-Description is deprecated with reference to the updated
Self-Description.

Revoked: GAIA-X needs to protect against bad actors that might not be able or
willing to correct false information. Hence a revocation mechanism is put into
place by which Self-Descriptions can be marked non-active. Revocation can be
performed by the original issuer of a Self-Description and also by trusted
parties. Each Catalogue decides for himself which issuers of revocation messages
are trusted.

In addition, the Self-Description has a "verification lifecycle" that is
internal to the individual Catalogue. The state in the verification lifecycle is
contingent on the result of the integrity checks and validation described below.

- Stakeholders
  
  - Issuer vs. Owner vs. Provider
  - Federators
  - Certification Assessment Bodies

## Self-Description Lifecycle Management

As described in the Architecture Document, there are four lifecycle states of Self-Descriptions.

- active
- eol (end of life after a timeout date)
- deprecated
- revoked

The lifecycle state applies to an entire Self-Description.
Only active Self-Descriptions are used to answer queries from the Catalogue.

Self-Descriptions can be protected with cryptographic signatures. Hence they are immutable and cannot be modified.
The lifecycle state is part of additional metadata maintained in the Catalogue.

### End-of-Life

Every Self-Description has a creation date included as an attribute.
A Catalogue defines for himself a maximum lifetime of Self-Descriptions before they are eol'ed.

### Deprecation

There can be only one Self-Description at a time for the same entity.
A newer valid (trusted) Self-Description for an entity with the same Identifier automatically deprecates previous ones.

### Revocation

### Self-Description Verification

A Self-Description that is either inconsistent or untrusted is not accepted into or removed from the local SD Storage.
If parts of Self-Description are inconsistent or untrusted, then the entire Self-Description is considered inconsistent or untrusted.

#### Trust Anchors

Every Catalogue stores a local trust-list of trust anchors. This is simply a list of Identifiers of GAIA-X Participants.
Every Participant in the trust list must have a valid Self-Description for itself in the Catalogue.
The local trust-list is accessible to users via the Catalogue API.

#### Consistency Checks

The content of the Self-Description is in the format of a Verifiable Credential.
This adds additional layers of "wrappers" and cryptographic signatures for elements inside a Self-Description.

For the consistency checks (in contrast to the trust checks), the Self-Description is "flattened" to a list of attributes.
This flattened list of attributes is the checked against the SHACL shapes of the Self-Description schemas indicated by it.

- Verify that all mandatory attributes and relations from the indicated Self-Description schemas are present.
- Verify that only defined attribtes and relations from the indicated schemas are used.
- Verify that all attributes and relations conform to the datatypes and constraints defined in the indicated schemas.
- Every possible Self-Description schema (except the schema for Participants) defines a mandatory “providedBy” attribute.
  The "providedBy" attribute of a Self-Description must point to the Self-Description of a GAIA-X Participant.
  There must be an active Self-Description for that Participant already in the Catalogue.

#### Trust Checks

A Self-Description is a Verifiable Presentation in JSON-LD format.
The Verifiable Presentation is generated by the Provider of the entity that is described.
The Provider also cryptographically signs the Self-Description.

The Verifiable Presentation can contain Verifiable Credentials that are signed by third parties, usually an accredited Conformance Assessment Body (CAB). (This includes GAIA-X labels.)

- Verify that the issuer of the Self-Description is the Participant referenced in the "providedBy" attribute.
- Verify that the public key of the issuer can be trusted (certificate with the public key verified by a trust anchor).
  - (Check with the CAB / Registry for that / Cached public keys?)
- Verify that a proof (i.e., a signature) created by the provider is present on the overall Verifiable Presentation (the outer structure of every Self-Description).
- Validate that the Self-Description was not altered by verifying the hash in its proof.
- Check that those attributes that need to be signed by a trusted party (e.g. certifications) are in a dedicated Verificable Credential inside the Self-Description.

For all Verifiable Credentials (VC) inside the Self-Description check the following:
- Verify that the VC was signed by a Participant on the trust list.
- Verify that the claims made in the VC are in the scope of competency of the CAB.

## Access to Raw Self-Descriptions and Meta-Data

Self-Descriptions are identified by their SHA256 hash.
Self-Descriptions can be retrieved in their raw JSON-LD from the Catalogue.

## Schema Management

## User Management (for Privately Operated Catalogues)

## Self-Description Validation Sandbox

To test the integrity and validation checks before submitting an SD
