# Self-Description Definition

Gaia-X Self-Descriptions describe Entities from the Gaia-X Conceptual Model in a machine interpretable format.
This includes Self-Descriptions for the Participants themselves, as well as the Resources and Services from the Providers.
Standardized Self-Description Schemas (which can be extended by the Federations for their domain) enable harmonized tooling and querying to find Entities.

```mermaid
flowchart LR
    entity[Entity]
    sd[Self-Descriptions]
    schema[Schema]
    entity -- described by --> sd
    sd -- validated against --> schema
```

Self-Descriptions in combination with trustworthy verification mechanisms empower Participants in their decision-making processes.
Specifically, Self-Descriptions can be used for:

- Tool-assisted evaluation, selection, composition and orchestration of Services and Resources
- Enforcement, continuous validation and trust monitoring together with usage policies
- Negotiation of contractual terms

The Participants (particularly Providers) are responsible for the creation of their Self-Descriptions.
In addition to self-declared information by Participants about themselves or their offerings, a Self-Description may comprise elements issued and signed by trusted parties.

## Self-Description Structure

<!--- Assembly Model -->
Self-Descriptions are [W3C Verifiable Presentations](https://www.w3.org/TR/vc-data-model/#presentations) in the [JSON-LD format](https://www.w3.org/2018/jsonld-cg-reports/json-ld).
A such, a Self-Description is mainly a list of so-called *Claims* in the form of [Verifiable Credentials](https://www.w3.org/TR/vc-data-model/#credentials).
The claims contain the "payload" of the Self-Description: A description of the Entity and relations to other Entities in the RDF data model.

```mermaid
graph LR
subgraph Self-Description
    subgraph Verifiable Presentation
        metadata1[Metadata]
        vc1[Verifiable Credential - 1..*]
        proof1[Proof Info - 1..*]
    end

    subgraph Verifiable Credential
        metadata2[Metadata]
        claim[Claim - 1..*]
        proof2[Proof Info - 1..*]
    end

    vc1 -- 1 .. * --> claim
end
```
*Self-Description assembly model*

<!--- Signatures -->
The overall Self-Description must be cryptographically signed by the Participant that is issuing it.
Each claim can be individually signed by its issuer (this can be a trusted party different from the issuer of the overall Self-Description).
Below is an example where a trusted party "DUV" asserts that a resource is certified according to ISO 27001.

```mermaid
graph TB
    subgraph Credential Graph
        cred123[Credential 123] -- credentialSubject --> node[Node]
        cred123 -- issuer --> duv[DUV]
        node -- hasCertificate --> iso(ISO 27001)
    end

    subgraph Proof Graph
        sig456[Signature 456] -- creator --> jane[Jane Doe]
        sig456 -- created --> date(2021-03-01 14:01:46)
    end

    duv -- proof ---> sig456

    %% classDef attribute fill:#f9f,stroke:#333,stroke-width:4px,rx:50%,ry:50%; %%
    classDef attribute stroke-width:2px,rx:50%,ry:50%;
    class iso,date attribute;
```
*Linked Claim statements as a graph representation*

<!--- Triples -->
Claims follow the subject-predicate-object structure of the RDF data model.
Leaving out the syntactic sugar of JSON-LD, the following triples represent that *BobsDB* is a database hosted by *HosterA* which supports at most 200 database tables and is certified by ISO 27001.

    (BobsDB, isA, Database)
    (BobsDB, hostedBy, HosterA)
    (BobsDB, maxTables, 200)
    (BobsDB, certifiedBy, ISO27001)
    
The possible attributes and relations to be used in a Self-Description come from Self-Description Schemas (see below).

<!--- Identifiers -->
Cross-referencing between Self-Descriptions is enabled by unique GAIA-X Identifiers for the Entities.
Identifiers in Gaia-X are URIs and follow the specification of RFC 3986.
Depending on the prefix of the URI, different technical systems are defined to ensure uniqueness of Identifiers.
For example, the use of a domain-name as part of the Identifier, where only the owner of the domain-name shall create Identifiers for it.
See the section on [Identity and Access Management](federation_service.md#identity-and-access-management) for more details.

<!--- Anonymous Entities -->
Every Self-Description has one Entity as its main topic.
This Entity must have a GAIA-X Identifier.
Self-Descriptions can additionally describe "anonymous entities" if they are required for the Self-Description and if these Entities do not yet have their own Self-Description.
The main reason for this is to give mandatory information which would go into a dedicated Self-Description, but which is not available so far.
(Take as an example that the provider company of the hosting infrastructure must be described).
These "anonymous entities" are defined via blank-nodes in the RDF data model and do not have identifiers.
Anonymous entities from different Self-Descriptions are not merged when they are loaded into a joint Self-Description Graph (see below).
So there can be duplicate anonymous entities.

### A Minimal Complete Self-Description

Below the example of Bob's Database is rendered in a valid Self-Description.

    {
     "@context": [
         "https://www.w3.org/2018/credentials/v1",
         "https://www.w3.org/2018/credentials/examples/v1"
     ],
    "id": "gaia-x:my-provider:my-id",
    "type": ["VerifiablePresentation", "CredentialManagerPresentation"],
    "verifiableCredential": [{}],
    "proof": [{}]
    }
*A minimal complete Self-Description*

The validation checks for the Self-Description can be performed with the following information:
The used Self-Description Schema can be downloaded at [https://xxx](https://xxx).
The public key used for the signature (in base64 encoding) is XXX for Bob and XXX for the DUV trusted party.
See the section below for the details on cryptographic signatures.

## Self-Description Schemas

The RDF 1.1-standard is used to express an extensible hierarchy of schemas
for Self-Descriptions with well-known attributes.

The Gaia-X Federation Services specification describes how core Self-Description schemas based on the
Conceptual Model are created and maintained. Individual Gaia-X Ecosystems can extend the schema
hierarchy for their application domain. Such extensions must make an explicit reference to the
organization that is responsible for the development and maintenance of the extension.

To foster interoperability, Self-Description Schemas are defined. They introduce classes, i.e., sets of
instances of the same type, which may form an extensible hierarchy. For each class, properties are
defined that an instance of the class can have. These include attributes, having immediate literal values
of some datatype (called “datatype properties” in the W3C Web Ontology Language OWL ), values that
are instances of auxiliary classes (e.g., a class that groups all information related to the deployment of a
Service as a distinct node in the graph), or reusable values that are instances of concepts from controlled
vocabularies (the latter two being called “object properties” in OWL). Properties also include
relationships, having values that are instances of some other class in the Gaia-X Conceptual model (e.g.,
the relationship between an Asset and its Provider, also OWL object properties). Self-Description
Schemas should include shapes , i.e., sets of conditions used to validate whether a Self-Description has
instantiated classes and properties according to their definition. In particular, shapes should specify
which properties are mandatory to be used by every instance of a given class, and which ones are
optional. A Self-Description has to state which schemas are used in its metadata. Only properties and
relations defined in these schemas must be used.

Gaia-X aims at building upon existing schemas, preferably those that have been standardized or at least
widely adopted to get a common understanding of the meaning and purpose of any property and Claim
statements. Examples of attribute categories per Self-Description in Gaia-X are discussed in the Appendix
A1.

For frequently used attribute values, it is recommended that they be maintained in the same governed
process as Self-Description Schemas, i.e., by giving them unambiguous identifiers maintained in
Controlled Vocabularies. Examples include standards against which a Participant or an Asset has been
certified, or classification schemes, e.g., for the sector in which a Provider is doing their business. It is
recommended to reuse Controlled Vocabularies where they already exist, e.g., ECLASS to identify
products and services. The W3C SKOS Simple Knowledge Organization System provides a way of
managing Controlled Vocabularies in a way that is compatible with the RDF data model and thus with
Self-Description Schemas.

In addition to Self-Descriptions, a Federated Catalogue also manages Self-Description Schemas.
A Federated Catalogue should only accept the submission of Self-Descriptions that validate against a Schema known to the Catalogue, as specified below.
Gaia-X develops an extensible hierarchy of Schemas that define the terms used in Self-Descriptions.
Some Schemas are standardized by the Gaia-X AISBL and must be supported by any Catalogue.
It is possible to create additional Schemas specific to an application domain, an Ecosystem, Participants in it, or Resources offered by these Participants.
Schemas have the same format as Self-Descriptions, i.e., they are graphs in the RDF data model, serialized as JSON-LD.
A Schema may define terms (classes, their attributes, and their relationships to other classes) in an ontology.
If it does, it must also define shapes to validate instances of the Ontology against.

The RDF graph that forms one Self-Description must only include the one Gaia-X Entity that is being described.  The shape of one Self-Description's graph must be a tree with that entity being the root node.  For complex attributes of the Entity, which require a structured representation rather than flat key-value pairs (i.e., in the RDF data model, predicate-object pairs), additional nodes may be included into one Self-Description's graph.  To avoid conflicts when merging such Self-Descriptions with others in the Self-Description Graph of a Federated Catalogue, it is recommended to not identify such nodes using URIs (which might clash with other URIs), but to use _blank nodes_, i.e., anonymous entities. Any URI that is not the identifier of the main GAIA-X Entity of the Self-Description may be replaced by an anonymous blanc node identifier in the Self-Description Graph.

In principle, either ServiceA or DataCenterB could state this relation.
It makes more sense if ServiceA indicates where it is hosted, as probably many services are probably hosted on DataCenterB.
This reduces the update frequency on the Self-Description of DataCenterB.

The following rules indicates which Self-Description should state the relation:
The Self-Description Schemas are defined in such a way that a Self-Description states its outgoing "forward references".
In the above example, "isDeployedOn" would be a forward reference and "hasDeployed" the corresponding inverse reference.

The organization providing the target Entity of a relation currently has no mechanism to prevent such relations being defined.
Additional definitions to that end may be defined in the future.

In addition to Self-Descriptions, a Federated Catalogue also manages Self-Description *Schemas*.
A Federated Catalogue shall only accept the submission of Self-Descriptions that validate against a Schema known to the Catalogue, as specified below.
Gaia-X develops an extensible hierarchy of Schemas that define the classes with their properties (attributes and relations) to be used in Self-Descriptions.
Some Schemas are standardized by the Gaia-X AISBL and defined in the Architecture Document or in a future Self-Description Document, which a future Architecture Document may point to.  These Schemas must be supported by any Catalogue.
It is possible to create additional Schemas specific to an application domain, an Ecosystem, Participants in it, or Resources offered by these Participants.

Multiple inheritance is allowed and encouraged in the schema hierarchy as well as for the instances in a Self-Description.
That way, deeply nested specializations in the schema hierarchy can be avoided.
For example, a REST-based database service could inherit from both *database* and *REST-based service* instead of creating a specialized class.

Attribute properties point to either of
- immediate literal values of some datatype (called "datatype properties" in the W3C Web Ontology Language OWL[^1001]),
- values that are instances of auxiliary classes (e.g., a class that groups all information related to the deployment of a Service as a distinct node in the graph), or
- reusable values that are instances of concepts from controlled vocabularies (the latter two being called "object properties" in OWL).
Relationship properties point to other instances via their Gaia-X identifier (e.g., the relationship between a Service Offering and its Provider; these are also OWL object properties).

Self-Description Schemas include shapes[^1002], i.e., sets of conditions used to validate whether a Self-Description has instantiated classes and properties according to their definition.
In particular, shapes specify which properties are mandatory to be used by every instance of a given class, and which ones are optional.

[^1001]: W3C (2012).  Web Ontology Language (OWL).  https://www.w3.org/OWL/

[^1002]: Shapes Constraint Language (SHACL).  W3C Recommendation 20 July 2017.  https://www.w3.org/TR/2017/REC-shacl-20170720/

## Signed Claims and Self-Descriptions


A attribute's Verifiable Credential is Gaia-X conformant if the Issuer of the Verifiable Credential has itself an identity coming from one of the Trust Anchors.

```mermaid
flowchart LR
    part["Participants"]

    subgraph ta[Trust Anchors]
        tsp{{Trust Service Provider}}
    end

    subgraph sd[Self-Descriptions]
        cred[Credentials]
    end

    tsp -- proves identity of --> part

    part -- hold --> sd
    part -- issue --> cred
    part -- verify --> cred
```

A Label is Gaia-X conformant if the Issuer of the Credential is one of the Trust Anchors' Gaia-X Label Issuers.

```mermaid
flowchart LR
    part["Participants"]

    subgraph ta[Trust Anchors]
        cab{{Gaia-X Label Issuers}}
    end

    subgraph sd[Self-Descriptions]
        cred["Gaia-X Labels<br>&#40;Credentials&#41;"]
    end

    part -- hold --> sd
    cab -- issue --> cred
    part -- verify --> cred
```

## Self-Description Graph

In order to search within a collection of (many) available Self-Descriptions, appropriate query mechanisms are put into action.
Typically the available Self-Descriptions are combined into a single “graph” for efficient querying and the resolution of cross-references.

The relations between Self-Descriptions form a graph with typed edges, which is called the Self-
Description Graph. The Catalogues implement a query algorithm on top of the Self-Description Graph.
Furthermore, Certification aspects and Usage Policies can be expressed and checked based on the Self-
Description Graph that cannot be gained from individual Self-Descriptions. For example, a Consumer
could use Catalogue Services to require that a Service Instance cannot depend on other Service
Instances that are deployed on Nodes outside a Consumer-specified list of acceptable countries.

Self-Descriptions contain typed relations to Entities from other Self-Descriptions.
They do so by referring to the GAIA-X identifiers.
These relations between Entities may cross organizational boundaries.
For example when `ServiceA` is hosted on `DataCenterB` from a different company.

These relations have a direction.
Self-Descriptions shall only contain "forward references".
Of course, the reverse direction can be queried in a system that evaluates the Self-Descriptions.
For example `(ServiceA, hostedOn, DataCenterB)` is described in the Self-Description of `ServiceA`.
The Self-Description of `DataCenterB` does not contain the reverse relation `(DataCenterB, hosts, ServiceA)`.
Which direction to define as the "forward direction" is considered in the definition of the Self-Description Schemas.

### Representing Claims in the Self-Description Graph

It is not immediately evident how the claim structure can be represented as “meta information” to be usable for querying.
The claim structure is preserved when importaing self-descriptions in the graph by adding so-called "edge properties".
That is, the origin of an attribute or relation is stored in the respective "edges" of the graph.
This is possible in Labeled-Property Graph Databases (e.g. based on GQL/OpenCypher) as well as in semantic triple-stores that support the RDF*/SPARQL* extension.

Labeled-Property Graph Representation
Labeled-Property Graphs allow “edge properties”. The edge properties are in bold font below.
(Bob, hasCar[claimedBy: Alice], redCar)
(redCar, hasColor[claimedBy: Alice], „red“)

RDF* Representation
In RDF*, edge properties are added by using the respective triples as the subject of another triple.
(Bob, hasCar, redCar)
(redCar, hasColor, “red”)
((Bob, hasCar, redCar), claimedBy, Alice)
((redCar, hasColor, “red”), claimedBy, Alice)

Queries
	Select all people with red cars	Select all cars verified by Alice
GQL	MATCH (person)-[:hasCar]->(car)
WHERE car.hasColor = “red”
RETURN person, car	MATCH (person)-[rel:hasCar]->(car)
WHERE rel.claimedBy = Alice
RETURN person, car
SPARQL*	SELECT ?person, ?car
WHERE {
    ?person hasCar ?name .
    ?car hasColor “red” .
}	SELECT ?person, ?car
WHERE {
    <<?person hasCar ?car>>
          claimedBy Alice .
}

The approach has the advantage that users can both write “easy queries” that don’t consider the claim structure information and “complex queries” that take it into account.
The “easy queries” are not impacted by the presence (or absence) of claim structure information.

The disadvantage is that fewer query languages allow “edge properties”.
For example, SPARQL* (in draft status) would have to be used instead of SPARQL.
Another disadvantage is that only claims “one level deep” can be represented straightforwardly via edge properties.
Deeply nested claims would require a more involved representation.
